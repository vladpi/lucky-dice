# Lucky Dice 
## Виртуальные игровые кости

Учебный проект на PHP, HTML и CSS.

Запущено при помощи Apache [на сервере](https://lucky-dice.vladpi.me)

Пример конфигурации виртуального хоста Apache находится в файле [lucky-dice-apache.conf](lucky-dice-apache.conf)
