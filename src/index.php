<!doctype html>
<html lang="ru" class="h-100">
<head>
    <?php include("blocks/head.php"); ?>
</head>
<body class="d-flex flex-column h-100">

<div class="row mb-3">
    <div class="col">
        <?php include("blocks/header.php"); ?>
    </div>
</div>

<div class="container">

    <div class="row mt-3 mb-4">
        <div class="col">
            <h1 class="h3"><b>«Счастливый кубик»</b> – сервис бросков виртуальных игровых кубиков</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a class="btn btn-outline-dark btn-lg btn-block " href="d4.php" role="button">
                <img src="assets/img/d4.png" height="80">
            </a>
        </div>
        <div class="col">
            <a class="btn btn-outline-dark btn-lg btn-block" href="d6.php" role="button">
                <img src="assets/img/d6.png" height="80">
            </a>
        </div>
        <div class="col">
            <a class="btn btn-outline-dark btn-lg btn-block" href="d8.php" role="button">
                <img src="assets/img/d8.png" height="80">
            </a>
        </div>
        <div class="col">
            <a class="btn btn-outline-dark btn-lg btn-block" href="d12.php" role="button">
                <img src="assets/img/d12.png" height="80">
            </a>
        </div>
        <div class="col">
            <a class="btn btn-outline-dark btn-lg btn-block" href="d20.php" role="button">
                <img src="assets/img/d20.png" height="80">
            </a>
        </div>
    </div>
</div>

<?php include("blocks/footer.php"); ?>

<?php include("blocks/scripts.php"); ?>

</body>
</html>