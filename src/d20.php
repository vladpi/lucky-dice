<!doctype html>
<html lang="ru" class="h-100">
<head>
    <?php include("blocks/head.php"); ?>
</head>
<body class="d-flex flex-column h-100">

<div class="row mb-3">
    <div class="col">
        <?php include("blocks/header.php"); ?>
    </div>
</div>

<div class="container">

    <div class="row mt-3 mb-4">
        <div class="col">
            <h1 class="h3"><img src="assets/img/d20.png" height="50"> <b>D20</b></h1>
        </div>
    </div>

    <?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $result = random_int(1, 20);
        echo "<h3 class='mb-3'>Результат броска: <b>$result</b></h3>";
    }
    ?>

    <form action="" method="post">
        <button type="submit" class="btn btn-outline-dark btn-lg btn-block">
            <?php
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                echo "Бросить еще!";
            } else {
                echo "Бросить!";
            }
            ?>
        </button>
    </form>

</div>

<?php include("blocks/footer.php"); ?>

<?php include("blocks/scripts.php"); ?>

</body>
</html>